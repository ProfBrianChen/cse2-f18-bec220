//Bennett Cole, 9/4/2018, CSE02 Section 210

public class WelcomeClass{
  public static void main (String args []){
    // For this assignemnt I just went line by line from top to bottom of the design. I copied and pasted from the googledoc and formatted as necessary
    System.out.println ("-----------");
    System.out.println ("| WELCOME |");
      System.out.println ("-----------");
    System.out.println ("^  ^  ^  ^  ^  ^");
    System.out.println ("/ \\/ \\/ \\/ \\/ \\/ \""); 
    //I was getting error messages about the escape key so I had to put in 2 backslashes so it would compile correctly
    System.out.println ("<-B--E--C--2--2--0->");
    System.out.println ("\\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println ("v  v  v  v  v  v");
    System.out.println ("I am 21 years old. I am from Wayland, Massachusetts");                    
  }
}
                        