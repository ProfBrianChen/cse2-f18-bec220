//Bennett Cole, CSE2 section 210, 9/24/2018

import java.util.Scanner;
public class CrapsIf {
  public static void main(String args[]){ 
    //Initialize the 2 die
    //I assigned a value to them as a placeholder because the code will not compile otherwise
    int Dice1 = (int)(Math.random ()*(7))+1;
    int Dice2 = (int)(Math.random ()*(7))+1;
    int answer;
    
    Scanner myScanner = new Scanner( System.in );
    
    System.out.println ("If you would like to input values type 1, if you would like to randomly generate values type 2");
    answer = myScanner.nextInt ();
    
    if (answer==1){
      System.out.println ("Type in a value for dice 1 between 1 and 6 ");
      Dice1 = myScanner.nextInt();
      System.out.println ("Type in a value for dice 2 between 1 and 6 ");
        Dice2 = myScanner.nextInt();
    }
    
    else if (answer==2){
      
      Dice1 = (int)(Math.random ()*(7))+1;
     Dice2 = (int)(Math.random ()*(7))+1;
    }
    else {
      System.out.println ("Invalid answer");
    }
    
   
    
    
    //Write if statements for every scenario
    //If its possible to get the same result 2 ways then there has to be an || between the two results
    
    if ((Dice1==1)&&(Dice2==1)) {
      System.out.println ("Snake eyes");
    }
    else if (((Dice1==1)&&(Dice2==2))||((Dice1==2)&&(Dice2==1))) {
      System.out.println ("Ace Deuce");
    }
    else if (((Dice1==1)&&(Dice2==3))||((Dice1==3)&&(Dice2==1))){
      System.out.println ("Easy 4");
    }
    else if (((Dice1==1)&&(Dice2==4))||((Dice1==4)&&(Dice2==1))){
      System.out.println ("Fever five");
    }
    else if (((Dice1==1)&&(Dice2==5)||(Dice1==5)&&(Dice2==1))){
      System.out.println ("Easy six");
    }
    if (((Dice1==1)&&(Dice2==6))||((Dice1==6)&&(Dice2==1))){
      System.out.println ("Seven out");
    }
    else if ((Dice1==2)&&(Dice2==2)) {
      System.out.println ("Hard four");
    }
     else if (((Dice1==2)&&(Dice2==3))||((Dice1==3)&&(Dice2==2))) {
       System.out.println ("Fever five");
     }
     else if (((Dice1==2)&&(Dice2==4))||((Dice1==4)&&(Dice2==2))) {
       System.out.println ("Easy six");
     }
     else if (((Dice1==2)&&(Dice2==5))||((Dice1==5)&&(Dice2==2))) {
       System.out.println ("Seven out");
     }
     else if (((Dice1==2)&&(Dice2==6))||((Dice1==6)&&(Dice2==2))) {
       System.out.println ("Easy eight");
     }
    else if ((Dice1==3)&&(Dice2==3)) {
      System.out.println ("Hard six");
    }
    else if (((Dice1==3)&&(Dice2==4))||((Dice1==4)&&(Dice2==3))) {
      System.out.println ("Seven out");
    }
    else if (((Dice1==3)&&(Dice2==5))||((Dice1==5)&&(Dice2==3))) {
      System.out.println ("Easy eight");
    }
    else if (((Dice1==3)&&(Dice2==6))||((Dice1==6)&&(Dice2==3))) {
      System.out.println ("Nine");
    }
    
    else if ((Dice1==4)&&(Dice2==4)) {
      System.out.println ("Hard eight");
    }
    else if (((Dice1==4)&&(Dice2==5))||((Dice1==5)&&(Dice2==4))) {
      System.out.println ("Nine");
    }
    else if (((Dice1==4)&&(Dice2==6))||((Dice1==6)&&(Dice2==4))) {
      System.out.println ("Easy Ten");
    }
    else if ((Dice1==5)&&(Dice2==5)) {
      System.out.println ("Hard Ten");
    }
    else if (((Dice1==5)&&(Dice2==6))||((Dice1==6)&&(Dice2==5))) {
      System.out.println ("Yo-leven");
    }
    else if ((Dice1==6)&&(Dice2==6)) {
      System.out.println ("Boxcars");
    }
    
    
    
    
  }
}

