//Bennett Cole, 9/5/2018, Section 210
//Program measures time elapsed in seconds and the rotation of the fron wheels on the bike ride

public class Cyclometer{
  public static void main (String args []){
    int secsTrip1 = 480;
    int secsTrip2 = 3220;
    //Measures the time elapsed in the trips
      int countsTrip1 = 1561;
      int countsTrip2 = 9037;
    //Measures the roatations of the front wheel in each corresponding trip
    double wheelDiameter = 27.0,
     PI = 3.14159,
     feetPerMile = 5280,
    inchesPerFoot = 12,
    secondsPerMinute = 60;
    double distanceTrip1, distanceTrip2, totalDistance;
    // our intermediate variables and output data.
    System.out.println ("Trip 1 took: " + secsTrip1/secondsPerMinute + " minutes and had " + countsTrip1 + " counts");
    System.out.println ("Trip 2 took "+ secsTrip2/secondsPerMinute + " minutes and had " + countsTrip2 + " counts");
    // Prints the time elasped in minutes and the revolutions of the wheel for each trip
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    // Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
distanceTrip1=(countsTrip1*wheelDiameter*PI)/inchesPerFoot/feetPerMile;
    distanceTrip2=(countsTrip2*wheelDiameter*PI)/inchesPerFoot/feetPerMile;
    totalDistance = distanceTrip1 + distanceTrip2;
    System.out.println ("Trip 1 was " + distanceTrip1 + " miles");
    System.out.println ("Trip 2 was " + distanceTrip2 + " miles"); 
    System.out.println ("Total distance was " + totalDistance + " miles");
    
  }
}

  
    
    
    