import java.util.Scanner;
public class Convert{
  public static void main (String[] args) {
    Scanner myScanner = new Scanner( System.in );
    System.out.print ("Enter the affected area in acres: ");
    double landAcres = myScanner.nextDouble();
    System.out.print ("Enter the rainfall in affected area: ");
    double rainFall = myScanner.nextDouble();
    //Convert acres to feet
    final double feetAcres = 43560.0;

    double squareFeetland = feetAcres*landAcres;
    //Convert inches of rain to feet
    double rainFallfeet = rainFall/12.0;
    //Compute the rain fall*acreage in terms of cubic feet
    double cubicRainfeet = squareFeetland*rainFallfeet;
    //Compute number of cubic feet in a mile
    final double feetCubicmiles = Math.pow (5280, 3.0);
    //Divide cubic feet of rain by cubic feet in a mile 
    double cubicRainmiles = (cubicRainfeet/feetCubicmiles);
    System.out.println (cubicRainmiles + " cubic miles");
    
    
    
    
    
    
  }
}