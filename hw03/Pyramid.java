import java.util.Scanner;
public class Pyramid{
  public static void main (String[] args) {
    Scanner myScanner = new Scanner ( System.in );
    System.out.print ("Enter pyramid base width: ");
      double baseWidth = myScanner.nextDouble ();
    System.out.print ("Enter pyramid base length: ");
    double baseLength = myScanner.nextDouble ();
    System.out.print ("Enter pyramid height: ");
    double pyramidHeight = myScanner.nextDouble ();
    //The equation for pyramid volume is (base length * base width * height)/3 so I just had to do that one computation
    double pyramidVolume = (baseLength*baseWidth*pyramidHeight)/3.0;
    System.out.println ("The volume of the pyramid is: " + pyramidVolume);
    
  }
}
    