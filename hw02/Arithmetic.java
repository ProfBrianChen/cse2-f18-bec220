//Bennett Cole, CSE002 Section 210, 9/9/2018

public class Arithmetic {
  public static void main (String args []) {
    //Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltCost = 33.99;


//the tax rate
double paSalesTax = 0.06;
    //total cost of each item
   double totalCostpants = numPants*pantsPrice;
   double totalCostshirts = numShirts*shirtPrice;
   double totalCostbelts = numBelts*beltCost;
    
  //total cost before tax 
    double totalCostpretax = totalCostpants + totalCostshirts + totalCostbelts;
    

   
    // total tax 
    double totalTax = totalCostpretax*paSalesTax;
    
    // total cost with tax
    double totalCost = totalTax + totalCostpretax;
    
    //adjust cost*tax so that the tax makes sense in dollar terms.
    double beltTax = (totalCostbelts*paSalesTax*100);
    double adjustedBelttax = ((int) beltTax)/100.0;
    double pantTax = (totalCostpants*paSalesTax*100);
    double adjustedPanttax = ((int) pantTax)/100.0;
    double shirtTax = (totalCostshirts*paSalesTax*100);
    double adjustedShirttax = ((int) shirtTax)/100.0; 
    
    //Adjust the total tax and total cost with tax 
    double totalAdjustedtax = adjustedShirttax + adjustedPanttax + adjustedBelttax;
    double totalAdjustedcost = totalCostpretax + totalAdjustedtax;
    
    
    System.out.println ("2 belts cost " + totalCostbelts + " before taxes and " + adjustedBelttax + " in taxes");
    System.out.println ("3 pants cost " + totalCostpants + " before taxes and "+ adjustedPanttax + " in taxes"); 
    System.out.println ("2 shirts cost " + totalCostshirts + " before taxes and "+ adjustedShirttax + " in taxes");
    System.out.println ("The total pretax cost is " + totalCostpretax + " and the total tax is " + totalAdjustedtax + " and the total cost with tax is " + totalAdjustedcost);  
  }
}
