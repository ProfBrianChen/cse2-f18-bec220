import java.util.Random;
  import java.util.Scanner; 
  public class hw09pt2{
  public static void main (String [] args){
    Random randGen = new Random ();
    Scanner myScanner = new Scanner (System.in);
    
    //The original array with 10 positions
    int [] mainArray = new int [10];
    //The array after 1 index is removed
    int [] mainArray2 = new int [9];
       String junk;
       //initializing the integers that will be assigned values from user input
       int g=0;
       int y=0;
       int u = 0;
       do{
         
         //print out the randomly generated value from randomInput
    mainArray = randomInput(mainArray);
  for (int i= 0; i<mainArray.length; i++){
           System.out.print (mainArray[i] + " ");
         }
  System.out.println();
  
  //Prompt the user for an index to remove and verify that they enter an integer that is within the bounds of the array
       System.out.println ("enter an index to remove");
       while (!myScanner.hasNextInt()){
         junk = myScanner.nextLine();
         System.out.println("integer please");
         
       }
       while (myScanner.hasNextInt()){
          y = myScanner.nextInt();
         if (y<0||y>9){
           System.out.println ("out of range");
           junk = myScanner.nextLine();
         while (!myScanner.hasNextInt()){
         junk = myScanner.nextLine();
         System.out.println("integer please");
         
       }
         }
         else{
           break;
         }
       }
       
       //Set the 2nd array equal to the array with 1 fewer indexes
       mainArray2 = delete (mainArray, y);
       
  
  
  
       System.out.println();
       //Check the user inputs to be sure that they are integers, and within the bounds of the array
       System.out.println("enter a value to remove");
       while (!myScanner.hasNextInt()){
         junk = myScanner.nextLine();
         System.out.println("integer please");
         
       }
       while (myScanner.hasNextInt()){
          g = myScanner.nextInt();
         if (g<0||g>9){
           System.out.println ("out of range");
         g = myScanner.nextInt();
         }
         else{
           break;
         }
       }
         
       
       remove ( mainArray2, g);
       System.out.println();
       
       
       
        
        
       //Give the user a terminal value to enter or allow them to run the program again if they choose. Ensure that the user enters an integer to prevent an exception
       System.out.println ("Type 1 to exit or type any other integer to go again");
       while (!myScanner.hasNextInt()){
         junk = myScanner.nextLine();
         System.out.println("integer please");
         
       }
       while (myScanner.hasNextInt()){
          u = myScanner.nextInt();
         break;
       }
       
  }while(u!=1);
  }
    public static int [] randomInput(int [] q){
      //import a random generator
      Random randGen = new Random ();
      //create an array that is the length of the array in the parameter
      int [] r = new int [q.length];
      //Randomly generate values from 0 to the length of the parameter array minus 1
      //int num1 = (randGen.nextInt (r.length));
      
      int [] array1 = new int [r.length];
      
      for (int i=0; i<array1.length; i++){
      array1[i]=  (randGen.nextInt (r.length));
      }
      
     
      
      
      
      
      
     return array1; 
    }
    
    
    
    public static int [] remove (int [] y, int p){
      
      //initialize k to act as a counter
      int k=0;
      //The for loop will run through the array to check for the value to remove
      for (int i =0; i<y.length; i++){
        if (y[i]==p){
          //Set the value of the index at the position to remove equal to a number that will not be randomly generated
          y[i]=100;
          //Increment the counter so that it can be subtracted from the  length of the original array when initializing the new array
          k+=1;
        }
      }
      
     
      //Index will fill in the indexes of the new array
      int index = 0;
      //the length of the new array will be the length of the original array minus the counter from the loop above
      int [] ynew = new int [y.length-k];
      //the for loop will run through the entire length of the original array
      for (int i=0; i < y.length; i++){
        //the positions of the value that the user chooses to remove will be sorted out
        //all the other values will be reinitialized in the new array, and their indexes will change if necessary
        if (y[i] != 100){
          ynew[index]=y[i];
          //increment the index with each loop
          index++;
        }
      }
      //print out the array
      for (int i = 0; i<ynew.length; i++){
        System.out.print(ynew[i] + " ");
      }
      
      
      return ynew;
    }
    
    public static int [] delete (int [] o, int u){
      for (int i=0; i<o.length; i++){
        //the value at the position that the user chooses to delete is set to a value that will not be randomly generated
        if (i==u){
          o[i]=-10;
        }
      }
      //The length of the new array will be one less than the length of the original array
      int [] t = new int [o.length-1];
      int index = 0;
      
      //The loop will run through the length of the original array and assign the values of the original array new indexes in accordance with the index that the user chose to delete
      for (int j=0; j<o.length; j++){
        if (o[j]!=-10){
          t[index] = o[j];
        index++;
        }
      }
        //Print out new array with selected values removed
        for (int p=0; p<t.length; p++){
          System.out.print (t[p] + " ");
        }
    
    
      
      return t;
    }
  }
    
  