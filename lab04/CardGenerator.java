public class CardGenerator {
  public static void main(String args[]){ 
    
    //Intialize the variables
    
    String cardIdentity = " ";
    int suit;
    String suitName = " ";
    
    //Create a random number generator than generates from 1 to 52
    
    suit = (int)(Math.random ()*(53))+1;
   
    //Create if statement to split the 52 possible integers into 4 groups that correspond to the 4 suits
    if ((suit >= 1)&&(suit <=13)) {
      suitName = "clubs";
    }
    else if ((suit >= 14)&&(suit<=26)) {
       suitName = "Hearts";
    }
    
    else if ((suit >= 26)&&(suit<=39)) {
       suitName = "Spades";
    }
    
    else if ((suit >= 40)&&(suit<=52)) {
       suitName = "Clovers";
    } 
    
      
    
    //Use the modulus to assign each of the thirteen cards in a given suit an identity from King through Ace
    switch (suit%13) {
   
      case 0:
    cardIdentity = "King";
    break;
    }
    switch (suit%12) {
        case 1:
        
        cardIdentity = "Queen";
        break;
       
    }
     switch (suit%11){   
        case 2:
         
         cardIdentity = "Jack";
         
    
        break;
     }
      switch (suit%10)  {
        case 3:
        cardIdentity = "ten";
          break;
      }
    
    switch (suit%9) {
      case 4:
        cardIdentity = "nine";
        break;
    }
    switch (suit%8) {
      case 5:
        cardIdentity = "eight";
        break;
    }
    switch (suit%7){
      case 6:
    cardIdentity = "seven";
    break;
    }
    
    switch (suit%6){
      case 7:
        cardIdentity = "six";
          break;
    }
    
   switch (suit%5){
     case 8:
    cardIdentity = "five";
      break;
   }
    switch (4%suit){
      case 9:
        cardIdentity = "four";
        break;
    }
    switch (suit%3){
      case 10:
        cardIdentity = "three";
        break;
    }
    switch (suit%2){
      case 11:
        cardIdentity = "two";
        break;
    }
    
    switch (suit%1){
      case 12:
        cardIdentity = "ace";
        break;
    }
    
    
    
    System.out.println (cardIdentity + " of " + suitName);
    
  
        
        
  
        
    }
    
        
      
    
  }
