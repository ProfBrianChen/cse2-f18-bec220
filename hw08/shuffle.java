import java.util.Random;
import java.util.Scanner;
public class hw08{
  public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
 String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 
String[] hand = new String[5]; 
int numCards = 0; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" "); 
} 
System.out.println();
//printArray(cards); 
shuffle(cards); 
//printArray(cards); 
while(again == 1){ 
   hand = getHand(cards,index,numCards); 
   System.out.println("Hand:");
   printArray(hand);
   index = index - numCards;
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt(); 
}  
  }
  public static void printArray (String [] r){
    //This method prints out the cards in order
    //The loop uses the String array cards as a parameter which incorporate both the card's suit and rank
    
    for (int i=0; i<r.length; i++){
      System.out.print (r[i] + " ");
    }
    System.out.println();
  }
  
  public static void shuffle (String [] r){
     Random randGen = new Random();
      
      String temp = "0"; 
       
        
        for (int j = 0; j<=100; j++){
          //Num1 generates a random value from 1 to 51
          //Num1 cannot generate index 0, because it is going to be used to replace the card at index 0
          int num1 = (randGen.nextInt (51))+1;
          //The string at index num1 is assigned to a placeholder variable, which takes it out of the deck
         temp = r[num1];
         //The value of the top card of the deck is assigned to index num1, this simulates the top card being put in the place of the randomly generated card
         r[num1]=r[0];
         //The value of the randomly generated card is assigned to index 0, this simulates the randomly generated card being put on top of the deck 
         r[0]=temp;
        
        }
        
        //The loop prints out the cards in the order that they have been shuffled
    System.out.println("Shuffled:");
        for (int k=0; k<r.length; k++){
        System.out.print(r[k] + " ");
        }
        System.out.println();
        
          
        }
  public static String [] getHand(String [] i, int k, int l){
   Scanner myScanner = new Scanner (System.in);
   
   //Determine the number of cards the user wants in the hand
   System.out.println("How many cards would you like in your hand");
    l=myScanner.nextInt();
    
String[] handlength = new String[l];
//This loop determines the cards to be printed
for (int j=0; j<handlength.length; j++){
  //The loop will run as many times as the length of the hand that the user requests
  //Each index of the array will be initialized to one of the values of String array cards
  //The index of string array cards will start at 51 and will be subtracted by the index of the string array handlength being initialized
  handlength[j]= i[k-j];
}
  
   
  //Returns the values for the hand to printarray in the main method
return handlength;
}
}




